package ru.leonova.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.leonova.tm.api.ServiceLocator;
import ru.leonova.tm.api.repository.IProjectRepository;
import ru.leonova.tm.api.repository.ITaskRepository;
import ru.leonova.tm.api.repository.IUserRepository;
import ru.leonova.tm.api.service.IProjectService;
import ru.leonova.tm.api.service.ITaskService;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.exeption.IllegalArgumentException;
import ru.leonova.tm.repository.ProjectRepository;
import ru.leonova.tm.repository.TaskRepository;
import ru.leonova.tm.repository.UserRepository;
import ru.leonova.tm.api.service.IUserService;
import ru.leonova.tm.service.ProjectService;
import ru.leonova.tm.service.TaskService;
import ru.leonova.tm.service.UserService;

import java.text.SimpleDateFormat;
import java.util.*;

public final class Bootstrap implements ServiceLocator {

    private final Scanner scanner = new Scanner(System.in);
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final ITaskRepository taskRepository = new TaskRepository();
    private final IUserRepository userRepository = new UserRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final ITaskService taskService = new TaskService(taskRepository, projectRepository);
    private final IUserService userService = new UserService(userRepository);
    private final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.leonova.tm").getSubTypesOf(AbstractCommand.class);
    @NotNull
    private final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public Bootstrap() {}

    @NotNull
    @Override
    public final List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @NotNull
    @Override
    public final IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public final IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public final ITaskService getTaskService() {
        return taskService;
    }

    private void registry(@NotNull final AbstractCommand command) throws Exception {
        @NotNull final String cliCommand = command.getName();
        @NotNull final String cliDescription = command.getDescription();
        if (cliCommand.isEmpty()) throw new Exception("It is not enumerated");
        if (cliDescription.isEmpty()) throw new Exception("Not description");
        command.setBootstrap(this);
        commands.put(cliCommand, command);
    }

    private boolean isCorrectCommand(@NotNull String command) {
        for (@NotNull final AbstractCommand c:commands.values()) {
            if (c.getName().equals(command)) {
                return true;
            }
        }
        return false;
    }

    private void registry(@NotNull final Set<Class<? extends AbstractCommand>> classes) throws Exception {
        for (Class clazz: classes) {
            registry(clazz);
        }
    }

    private void registry(@NotNull final Class... classes) throws Exception {
        for (@NotNull final Class clazz : classes) {
            if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
            @NotNull final Object command = clazz.newInstance();
            @NotNull final  AbstractCommand abstractCommand = (AbstractCommand) command;
            registry(abstractCommand);
        }
    }

    public void init() throws Exception {
        registry(classes);
        initUsers();
        start();
    }

    private void initUsers() {
        try {
            userService.adminRegistration("admin", "admin");
            @NotNull final User user = new User("l", userService.md5Apache("p"));
            userService.create(user);
            @NotNull final Date  dateStart = format.parse("2020-12-12");
            @NotNull final Date dateEnd = format.parse("2020-12-15");
            @NotNull final Project project = new Project("project", user.getUserId(), dateStart, dateEnd);
            projectService.create(user.getUserId(), project);
            @NotNull final Date  dateStart2 = format.parse("2020-12-12");
            @NotNull final Date dateEnd2 = format.parse("2020-12-15");
            @NotNull final Project project2 = new Project("project2", user.getUserId(), dateStart2, dateEnd2);
            projectService.create(user.getUserId(), project2);
            @NotNull final Date  dateTaskStart = format.parse("2021-01-01");
            @NotNull final Date dateTaskEnd = format.parse("2021-12-01");
            @NotNull final Task task = new Task("task1", project.getProjectId(), user.getUserId(), dateTaskStart, dateTaskEnd);
            taskService.create(user.getUserId(), task);
            @NotNull final Date  dateTaskStart2 = format.parse("2020-01-27");
            @NotNull final Date dateTaskEnd2 = format.parse("2020-02-27");
            @NotNull final Task task2 = new Task("task1", project.getProjectId(), user.getUserId(), dateTaskStart2, dateTaskEnd2);
            taskService.create(user.getUserId(), task2);
        } catch (Exception e) {
            System.out.println("Something went wrong in initUsers!");
            e.printStackTrace();
        }
    }

    private void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        try {

            @NotNull String command;
            do {
                command = scanner.nextLine();
                execute(command);
            } while (!command.equals("exit"));
        }catch (EmptyCollectionException | IllegalArgumentException e){
            System.err.println(e.getMessage());
        }
    }

    private void execute(@NotNull final String command) throws Exception {
        if (command.isEmpty() || !isCorrectCommand(command)) return;
        @NotNull final AbstractCommand abstractCommand = commands.get(command);
        @NotNull final boolean secureCheck = !abstractCommand.secure() || (abstractCommand.secure() && userService.isAuth());
        if (secureCheck || userService.getCurrentUser() != null) {
            abstractCommand.execute();
        } else {
            System.out.println("Log in for this command");
        }
    }

}

