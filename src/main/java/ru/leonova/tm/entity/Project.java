package ru.leonova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public final class Project {

    private  String name;
    private String projectId;
    private String description;
    private Date dateSystem;
    private Date dateStart;
    private Date dateEnd;
    private String userId;
    private String status;

    public Project(String name, String userId) {
        this.name = name;
        this.userId = userId;
        projectId = UUID.randomUUID().toString();
    }
    public Project(String name, String userId, Date dateStart, Date dateEnd) {
        this.name = name;
        this.userId = userId;
        projectId = UUID.randomUUID().toString();
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
    }
}
