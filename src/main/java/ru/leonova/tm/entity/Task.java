package ru.leonova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.leonova.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public final class Task {

    private String name;
    private String projectId;
    private String taskId;
    private String userId;
    private String description;
    private Date dateSystem;
    private Date dateStart;
    private Date dateEnd;
    private String status;

    public Task(String name, String prId, String userId) {
        this.name = name;
        this.projectId = prId;
        this.userId = userId;
        taskId = UUID.randomUUID().toString();
    }
    public Task(String name, String prId, String userId, Date dateStart, Date dateEnd) {
        this.name = name;
        this.projectId = prId;
        this.userId = userId;
        taskId = UUID.randomUUID().toString();
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.dateSystem = new Date();
    }
}

