package ru.leonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.enumerated.Status;
import ru.leonova.tm.exeption.IllegalArgumentException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Scanner;

public final class TaskCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "create-t";
    }

    @Override
    public String getDescription() {
        return "Create task";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }

    @Override
    public void execute() throws ParseException, IllegalArgumentException {
        System.out.println("[CREATE TASK]\nList projects:");
        Collection<Project> projectCollection = serviceLocator.getProjectService().getList();
        int i = 0;
        for (Project project : projectCollection) {
            i++;
            System.out.println(i + ". ID PROJECT: " + project.getProjectId() + ", NAME: " + project.getName());
        }
        System.out.println("\nSELECT ID PROJECT: ");
        String projectId = getScanner().nextLine();
        @NotNull User currUser = serviceLocator.getUserService().getCurrentUser();
        System.out.println("Enter name task: ");
        Scanner input = new Scanner(System.in);
        String name = input.nextLine();
        Task task = new Task(name, projectId, currUser.getUserId());
        task.setStatus(Status.PLANNED.getStatus());
        System.out.println("Date start task:");
        String date1 = getScanner().nextLine();
        System.out.println("Date end task:");
        String date2 = getScanner().nextLine();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        Date dateStart = format.parse(date1);
        Date dateEnd = format.parse(date2);
        task.setDateSystem(date);
        task.setDateStart(dateStart);
        task.setDateEnd(dateEnd);
        serviceLocator.getTaskService().create(currUser.getUserId(),task);
        System.out.println("Task created");

    }
}
