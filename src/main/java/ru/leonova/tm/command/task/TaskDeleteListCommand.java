package ru.leonova.tm.command.task;

import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.exeption.IllegalArgumentException;

public final class TaskDeleteListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "del-all-t";
    }

    @Override
    public String getDescription() {
        return "Delete list tasks of this user ";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }

    @Override
    public void execute() throws IllegalArgumentException, EmptyCollectionException {
        System.out.println("["+getDescription().toUpperCase()+"]");
        serviceLocator.getTaskService().deleteAllTaskByUserId(serviceLocator.getUserService().getCurrentUser().getUserId());
        System.out.println("All tasks remove");
    }
}
