package ru.leonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.exeption.IllegalArgumentException;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;

public class TaskSortByStatus extends AbstractCommand {
    @Override
    public String getName() {
        return "t-sort-by-status";
    }

    @Override
    public String getDescription() {
        return "Sorted projects by status";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws IllegalArgumentException, EmptyCollectionException {
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getUserId();
        @NotNull final List<Task> taskList = serviceLocator.getTaskService().sortTasksByStatus(userId);
        System.out.println("\nAfter Sorting by status:");
        @NotNull final SimpleDateFormat format = new SimpleDateFormat();
        for (@NotNull final Task task : taskList) {
            System.out.println(task.getName() + " " + format.format(task.getDateSystem()));
        }
    }
}
