package ru.leonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.system.AboutCommand;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.exeption.IllegalArgumentException;

import java.text.SimpleDateFormat;
import java.util.Collection;

public class TaskShowListByUserId extends AboutCommand {
    @Override
    public String getName() {
        return "list-t-u";
    }

    @Override
    public String getDescription() {
        return "Show tasks list for current user";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }

    @Override
    public void execute() throws IllegalArgumentException {
        if(serviceLocator.getTaskService().isEmptyTaskList()){
            System.out.println("List tasks is empty");
            return;
        }
        System.out.println("[TASK LIST]");
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getUserId();
        if (userId.isEmpty())return;
        @NotNull final Collection<Task> taskCollection = serviceLocator.getTaskService().findAllByUserId(userId);
        if (taskCollection.isEmpty())return;
        @NotNull final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        int i = 0;
        for (@NotNull final Task task : taskCollection) {
            i++;
            System.out.println(i + ". PROJECT ID: " + task.getProjectId() + ",TASK ID: " + task.getTaskId() + ", TASK NAME: " + task.getName()+ ", Date start: " + format.format(task.getDateStart()) + ", Date end: " +  format.format(task.getDateEnd()));
        }
    }
}
