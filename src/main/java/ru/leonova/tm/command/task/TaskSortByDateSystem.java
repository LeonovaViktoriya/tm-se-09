package ru.leonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.exeption.IllegalArgumentException;

import java.text.SimpleDateFormat;
import java.util.List;

public class TaskSortByDateSystem extends AbstractCommand {
    @Override
    public String getName() {
        return "t-sort-system-date";
    }

    @Override
    public String getDescription() {
        return "Sorted tasks by system date";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws EmptyCollectionException, IllegalArgumentException {
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getUserId();
        @NotNull final List<Task> taskList = serviceLocator.getTaskService().sortProjectsBySystemDate(userId);
        System.out.println("\nAfter Sorting by start date:");
        @NotNull final SimpleDateFormat format = new SimpleDateFormat();
        for (@NotNull final Task task : taskList) {
            System.out.println(task.getName() + " " + format.format(task.getDateSystem()));
        }
    }
}
