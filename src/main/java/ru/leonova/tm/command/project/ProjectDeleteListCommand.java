package ru.leonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.exeption.IllegalArgumentException;

public final class ProjectDeleteListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "del-all-p";
    }

    @Override
    public String getDescription() {
        return "Delete all projects";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws IllegalArgumentException, EmptyCollectionException {
        System.out.println("["+getDescription().toUpperCase()+"]");
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getUserId();
        serviceLocator.getProjectService().deleteAllProject(userId);
        serviceLocator.getTaskService().deleteAllTaskByUserId(userId);
    }
}
