package ru.leonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.exeption.IllegalArgumentException;

import java.text.SimpleDateFormat;
import java.util.Collection;

public final class ProjectShowListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "list-p";
    }

    @Override
    public String getDescription() {
        return "Show project list";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws IllegalArgumentException {
        System.out.println("[PROJECT LIST]");
        @NotNull final User user = serviceLocator.getUserService().getCurrentUser();
        @NotNull final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        @NotNull final Collection<Project> projectCollection;
        if(user.getRoleType().equals(RoleType.ADMIN.getRole())) projectCollection = serviceLocator.getProjectService().getList();
        else projectCollection = serviceLocator.getProjectService().findAllByUserId(user.getUserId());
        int i=0;
        for (@NotNull final Project project : projectCollection) {
            if(serviceLocator.getUserService().getCurrentUser().getUserId().equals(project.getUserId())) {
                System.out.println(++i + ". ID PROJECT: " + project.getProjectId() + ", NAME: " + project.getName() + ", ID USER: " + project.getUserId() + ", Date start: " + format.format(project.getDateStart()) + ", Date end: " +  format.format(project.getDateEnd()));
            }
        }
    }
}
