package ru.leonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.exeption.IllegalArgumentException;

import java.text.SimpleDateFormat;
import java.util.List;

public class ProjectSortByDateEnd extends AbstractCommand {
    @Override
    public String getName() {
        return "p-sort-start-date";
    }

    @Override
    public String getDescription() {
        return "Sorted projects by start date";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws EmptyCollectionException, IllegalArgumentException {
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getUserId();
        @NotNull final List<Project> projects = serviceLocator.getProjectService().sortProjectsByEndDate(userId);
        System.out.println("\nAfter Sorting by start date:");
        @NotNull final SimpleDateFormat format = new SimpleDateFormat();
        for (@NotNull final Project project : projects) {
            System.out.println(project.getName() + " " + format.format(project.getDateSystem()));
        }
    }
}
