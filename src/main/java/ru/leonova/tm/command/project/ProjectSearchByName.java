package ru.leonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.exeption.IllegalArgumentException;

public class ProjectSearchByName extends AbstractCommand {
    @Override
    public String getName() {
        return "search-p-by-name";
    }

    @Override
    public String getDescription() {
        return "Search project by name";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws IllegalArgumentException {
        System.out.println(getDescription());
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getUserId();
        System.out.println("\nEnter name project:");
        @NotNull final String projectName = getScanner().nextLine();
        @NotNull final Project project = serviceLocator.getProjectService().searchProjectByName(userId, projectName);
        System.out.println(project.getName()+project.getDateStart()+project.getDateEnd());
    }
}
