package ru.leonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.IllegalArgumentException;

import java.util.Collection;

public class ProjectUpdateStatusCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "up-p-s";
    }

    @Override
    public String getDescription() {
        return "Project Update Status";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws AccessException, IllegalArgumentException {
        System.out.println("[UPDATE STATUS PROJECT]\nList projects");
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getUserId();
        @NotNull final Collection<Project> projectCollection = serviceLocator.getProjectService().findAllByUserId(userId);
        int i = 0;
        for (Project project : projectCollection) {
            i++;
            System.out.println(i + ". ID PROJECT: " + project.getProjectId() + ", NAME: " + project.getName());
        }
        System.out.println("Enter id project:");
        @NotNull final String projectId = getScanner().nextLine();
        System.out.println("Enter new name for this project:");
        @NotNull final String name = getScanner().nextLine();
        serviceLocator.getProjectService().updateNameProject(userId, projectId, name);
    }
}
