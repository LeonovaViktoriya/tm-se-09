package ru.leonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.enumerated.Status;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.IllegalArgumentException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "create-p";
    }

    @Override
    public String getDescription() {
        return "Create project";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws ParseException, AccessException, IllegalArgumentException {
        if (!serviceLocator.getUserService().isAuth()){
            System.out.println("You are not authorized");
            return;
        }
        System.out.print("[CREATE PROJECT]\nINTER NAME: \n");
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getUserId();
        @NotNull final Project project = new Project(getScanner().nextLine(), userId);
        project.setStatus(Status.PLANNED.getStatus());
        System.out.println("date start project:");
        @NotNull final String date1 = getScanner().nextLine();
        System.out.println("date end project:");
        @NotNull final String date2 = getScanner().nextLine();
        @NotNull final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        @NotNull final Date date = new Date();
        @NotNull final Date  dateStart = format.parse(date1);
        @NotNull final Date dateEnd = format.parse(date2);
        project.setDateSystem(date);
        project.setDateStart(dateStart);
        project.setDateEnd(dateEnd);
        serviceLocator.getProjectService().create(userId, project);
        System.out.println("Project created");

    }
}
