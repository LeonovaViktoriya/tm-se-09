package ru.leonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.exeption.IllegalArgumentException;

public final class UserRegistrationCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "reg";
    }

    @Override
    public String getDescription() {
        return "Registration user";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws IllegalArgumentException {
        System.out.println("["+getDescription().toUpperCase()+"]\nEnter login:");
        @NotNull final String login = getScanner().nextLine();
        System.out.println("Enter password:");
        @NotNull String password = getScanner().nextLine();
        password = serviceLocator.getUserService().md5Apache(password);
        @NotNull final User user = new User(login, password);
        serviceLocator.getUserService().create(user);
        System.out.println("User is registered");
    }
}
