package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.ITaskRepository;
import ru.leonova.tm.api.service.ITaskService;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.exeption.IllegalArgumentException;

import java.util.Collection;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    final private ITaskRepository taskRepository;
    final private ru.leonova.tm.api.repository.IProjectRepository projectRepository;

    public TaskService(ITaskRepository taskRepository, ru.leonova.tm.api.repository.IProjectRepository iProjectRepository) {
        this.taskRepository = taskRepository;
        projectRepository = iProjectRepository;
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final Task task) throws IllegalArgumentException {
        if(userId.isEmpty()) throw new IllegalArgumentException();
        if (task.getUserId().equals(userId)) taskRepository.merge(task);
    }

    @NotNull
    @Override
    public Collection<Task> getList() {
        return taskRepository.findAll();
    }

    @Override
    public void updateTaskName(@NotNull String userId, @NotNull final String taskId, @NotNull final String taskName) throws IllegalArgumentException {
        if (taskName.isEmpty() || taskId.isEmpty() || taskId.equals(userId)) throw new IllegalArgumentException();
        @NotNull final Task task = taskRepository.findOne(taskId);
        if(task!=null) task.setName(taskName);
    }

    @Override
    public void deleteTasksByIdProject(@NotNull final String userId, @NotNull final String projectId) throws IllegalArgumentException, EmptyCollectionException, AccessException {
        if(projectId.isEmpty() || userId.isEmpty()) throw new IllegalArgumentException();
        @NotNull final Project project = projectRepository.findOneById(userId, projectId);
        if (project!=null && project.getUserId().equals(userId)) {
            @NotNull final Collection<Task> taskCollection = taskRepository.findAll();
            if(taskCollection==null) throw new EmptyCollectionException();
            taskCollection.removeIf(task -> task.getProjectId().equals(projectId));
        }
    }

    @Override
    public Collection<Task> findAllByUserId(@NotNull final String userId) throws IllegalArgumentException {
        if(userId.isEmpty()) throw new IllegalArgumentException();
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public Collection<Task> findAllByProjectId(@NotNull final String projectId) throws IllegalArgumentException {
        if(projectId.isEmpty()) throw new IllegalArgumentException();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public boolean isEmptyTaskList() {
        return taskRepository.findAll().isEmpty();
    }

    @Override
    public void deleteTask( @NotNull final String userId, @NotNull final String taskId) throws IllegalArgumentException, AccessException {
        if(taskId.isEmpty() || userId.isEmpty()) throw new IllegalArgumentException();
        @NotNull final Task task = taskRepository.findOne(taskId);
        if (task == null || task.getProjectId()==null || task.getProjectId().isEmpty()) return;
        @NotNull final Project project = projectRepository.findOneById(userId, task.getProjectId());
        @NotNull final String projectUserId = project.getUserId();
        if (!projectUserId.isEmpty() && project.getUserId().equals(userId)) taskRepository.remove(task);
    }


    @Override
    public void deleteAllTaskByUserId(@NotNull final String userId) throws IllegalArgumentException, EmptyCollectionException {
        if(userId.isEmpty()) throw new IllegalArgumentException();
        @NotNull final Collection<Task> taskCollection = taskRepository.findAllByUserId(userId);
        if(taskCollection==null || taskCollection.isEmpty()) throw new EmptyCollectionException();
        taskRepository.removeAllTasksCollection(taskCollection);
    }

    @Override
    public void deleteAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) throws IllegalArgumentException, EmptyCollectionException, AccessException {
        if(userId.isEmpty() || projectId.isEmpty()) throw new IllegalArgumentException();
        @NotNull final Project project = projectRepository.findOneById(userId, projectId);
        if(project!=null) throw new AccessException();
        @NotNull final Collection<Task> taskCollection = taskRepository.findAllByProjectId(projectId);
        if(taskCollection==null || taskCollection.isEmpty()) throw new EmptyCollectionException();
        taskRepository.removeAllTasksCollection(taskCollection);
    }

    @Override
    public Task findTaskByName(@NotNull final String userId, @NotNull final String taskName) throws IllegalArgumentException {
        if(userId.isEmpty() || taskName.isEmpty())throw new IllegalArgumentException();
        @NotNull final Task task = taskRepository.findOneByName(userId, taskName);
        if(task == null) return null;
        return task;
    }

    @Override
    public List<Task> sortTasksByEndDate(@NotNull final String userId) throws IllegalArgumentException, EmptyCollectionException {
        if(userId.isEmpty()) throw new IllegalArgumentException();
        return taskRepository.sortByEndDate(userId);
    }

    @Override
    public List<Task> sortProjectsByStartDate(@NotNull final String userId) throws IllegalArgumentException, EmptyCollectionException {
        if(userId.isEmpty()) throw new IllegalArgumentException();
        return taskRepository.sortByStartDate(userId);
    }

    @Override
    public List<Task> sortProjectsBySystemDate(@NotNull final String userId) throws IllegalArgumentException, EmptyCollectionException {
        if(userId.isEmpty()) throw new IllegalArgumentException();
        return taskRepository.sortBySystemDate(userId);
    }

    @Override
    public List<Task> sortTasksByStatus(String userId) throws IllegalArgumentException, EmptyCollectionException {
        if(userId.isEmpty()) throw new IllegalArgumentException();
        return taskRepository.sortByStatus(userId);
    }

}
