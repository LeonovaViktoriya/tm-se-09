package ru.leonova.tm.service;

import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.IUserRepository;
import ru.leonova.tm.api.service.IUserService;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.exeption.IllegalArgumentException;

import java.util.Collection;

public final class UserService extends AbstractService<User> implements IUserService {

    final private IUserRepository userRepository;
    private User currentUser;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void setCurrentUser(final User currentUser) {
        this.currentUser = currentUser;
    }

    @Override
    public void create(@NotNull final User user) {
        user.setRoleType(RoleType.USER.getRole());
        userRepository.persist(user);
    }

    @Override
    public Collection<User> getList() {
        return userRepository.findAll();
    }

    @Override
    public User authorizationUser(@NotNull final String login, @NotNull final String password) throws IllegalArgumentException {
        if (login.isEmpty() || password.isEmpty()) throw new IllegalArgumentException();
        @NotNull final Collection<User> userCollection = userRepository.findAll();
        for (@NotNull final User user : userCollection) {
            if (user.getLogin().equals(login) & user.getPassword().equals(md5Apache(password))) {
                currentUser = user;
                return currentUser;
            }
        }
        return null;
    }

    @Override
    public boolean isAuth() {
        return currentUser != null;
    }

    @Override
    public User getById(@NotNull final String userId) throws IllegalArgumentException {
        if (userId.isEmpty()) throw new IllegalArgumentException();
        return userRepository.findOne(userId);
    }

    public String md5Apache(@NotNull final String password) throws IllegalArgumentException {
        if(password.isEmpty()) throw new IllegalArgumentException();
        return DigestUtils.md5Hex(password);
    }

    public void adminRegistration(@NotNull final String login, @NotNull String password) throws IllegalArgumentException {
        if(login.isEmpty() || password.isEmpty()) throw new IllegalArgumentException();
        @NotNull final User admin = new User("admin", md5Apache("admin"));
        admin.setRoleType(RoleType.ADMIN.getRole());
        userRepository.persist(admin);
    }

}
