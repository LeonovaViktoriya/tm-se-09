package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.IProjectRepository;
import ru.leonova.tm.api.service.IProjectService;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.exeption.IllegalArgumentException;

import java.util.*;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    final private IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @NotNull
    @Override
    public Collection<Project> getList() {
        return projectRepository.findAll();
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final Project project) throws IllegalArgumentException, AccessException {
        @NotNull final String projectId = project.getProjectId();
        if(userId.isEmpty() || projectId.isEmpty()) throw new IllegalArgumentException();
        if(!project.getUserId().equals(userId)) throw new AccessException();
        if (isExist(projectId)) projectRepository.merge(project);
        else projectRepository.persist(project);
    }

    private boolean isExist(@NotNull final String projectId) throws IllegalArgumentException {
        if(projectId.isEmpty()) throw new IllegalArgumentException();
        return projectRepository.isExist(projectId);
    }

    @Override
    public void updateNameProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name) throws IllegalArgumentException, AccessException {
        if(userId.isEmpty() || projectId.isEmpty() || name.isEmpty()) throw new IllegalArgumentException();
        if (projectRepository.findOneById(userId, projectId) != null && projectRepository.findOneById(userId, projectId).getUserId().equals(userId)) {
            projectRepository.updateProjectName(userId, projectId, name);
        }
    }

    @Override
    public Collection<Project> findAllByUserId(@NotNull final String userId) throws IllegalArgumentException {
        if(userId.isEmpty()) throw new IllegalArgumentException();
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public void deleteProject(@NotNull final String userId, @NotNull final String projectId) throws IllegalArgumentException, AccessException {
        if(projectId.isEmpty() || userId.isEmpty()) throw new IllegalArgumentException();
        @NotNull final Project project = projectRepository.findOneById(userId, projectId);
        if (project != null && project.getUserId().equals(userId)) projectRepository.remove(userId, project);
    }

    @Override
    public void deleteAllProject(@NotNull final String userId) throws IllegalArgumentException, EmptyCollectionException {
        if(userId.isEmpty()) throw new IllegalArgumentException();
        projectRepository.removeAllProjectsByUserId(userId);
    }

    @Override
    public  List<Project> sortProjectsBySystemDate(@NotNull final String userId) throws IllegalArgumentException, EmptyCollectionException {
        if(userId.isEmpty()) throw new IllegalArgumentException();
        return projectRepository.sortedProjectsBySystemDate(userId);
    }

    @Override
    public List<Project> sortProjectsByStartDate(@NotNull final String userId) throws IllegalArgumentException, EmptyCollectionException {
        if(userId.isEmpty()) throw new IllegalArgumentException();
        return projectRepository.sortedProjectsByStartDate(userId);
    }

    @Override
    public List<Project> sortProjectsByEndDate(@NotNull final String userId) throws IllegalArgumentException, EmptyCollectionException {
        if(userId.isEmpty()) throw new IllegalArgumentException();
        return projectRepository.sortedProjectsByEndDate(userId);
    }

    @Override
    public List<Project> sortProjectsByStatus(@NotNull final String userId) throws EmptyCollectionException, IllegalArgumentException {
        if(userId.isEmpty()) throw new IllegalArgumentException();
        return projectRepository.sortedProjectsByStatus(userId);
    }

    @Override
    public Project searchProjectByName(@NotNull final String userId, @NotNull final String projectName) throws IllegalArgumentException {
        if(userId.isEmpty() || projectName.isEmpty()) throw new IllegalArgumentException();
        return projectRepository.searchProjectByName(userId, projectName);
    }

    @Override
    public Project searchProjectByDescription(@NotNull final String userId, @NotNull final String description) throws IllegalArgumentException {
        if(userId.isEmpty() || description.isEmpty()) throw new IllegalArgumentException();
        return projectRepository.searchProjectByDescription(description, userId);
    }
}
