package ru.leonova.tm.service;

import java.util.Collection;

abstract class AbstractService<E> {

    abstract Collection<E> getList();
}
