package ru.leonova.tm.exeption;

public class AccessException extends Exception {
    public AccessException(){
        super("Access denied!");
    }
    public AccessException(String message){
        super(message);
    }
}
