package ru.leonova.tm.exeption;

public class IllegalArgumentException extends Exception {
    public IllegalArgumentException(){
        super("Argument is empty!");
    }
    public IllegalArgumentException(String message){
        super(message);
    }
}
