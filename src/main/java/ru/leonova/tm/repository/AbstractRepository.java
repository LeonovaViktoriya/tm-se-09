package ru.leonova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.IllegalArgumentException;

abstract class AbstractRepository<E> {

    abstract void persist(@NotNull E e) throws IllegalArgumentException;

    abstract void merge(@NotNull E e) throws IllegalArgumentException, AccessException;
}
