package ru.leonova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.exeption.IllegalArgumentException;

import java.util.*;

public final class TaskRepository extends AbstractRepository<Task> implements ru.leonova.tm.api.repository.ITaskRepository {

    final private Map<String, Task> taskMap = new LinkedHashMap<>();

    @Override
    public void persist(@NotNull final Task task) throws IllegalArgumentException {
        if (task.getTaskId().isEmpty()) throw new IllegalArgumentException();
        taskMap.put(task.getTaskId(), task);
    }

    @Override
    public void merge(@NotNull final Task task) throws IllegalArgumentException {
        @NotNull final String taskId = task.getTaskId();
        if(taskId.isEmpty()) throw new IllegalArgumentException();
        if (isExist(taskId)){
            @NotNull final Task taskUpdate = taskMap.get(taskId);
            taskUpdate.setName(task.getName());
        }
        else persist(task);
    }

    @Override
    public boolean isExist(@NotNull final String taskId) throws IllegalArgumentException {
        if (taskId.isEmpty()) throw new IllegalArgumentException();
        return taskMap.containsKey(taskId);
    }

    @Override
    public Collection<Task> findAll(){
        return taskMap.values();
    }

    @Override
    public Task findOne(@NotNull final String taskId) throws IllegalArgumentException {
        if(taskId.isEmpty()) throw new IllegalArgumentException();
        return taskMap.get(taskId);
    }

    @Override
    public Collection<Task> findAllByUserId(@NotNull final String userId) throws IllegalArgumentException {
        if(userId.isEmpty()) throw new IllegalArgumentException();
        @NotNull final Collection<Task> taskCollection = new ArrayList<>();
        for (@NotNull final Task task: taskMap.values()) {
            if(task.getUserId().equals(userId)){
                taskCollection.add(task);
            }
        }
        return taskCollection;
    }

    @Override
    public Collection<Task> findAllByProjectId(@NotNull final String projectId) throws IllegalArgumentException {
        if(projectId.isEmpty())throw new IllegalArgumentException();
        @NotNull final Collection<Task> taskCollection = new ArrayList<>();
        for (@NotNull final Task task: taskMap.values()) {
            if(task.getProjectId().equals(projectId)){
                taskCollection.add(task);
            }
        }
        return taskCollection;
    }

    @Override
    public void remove(@NotNull final Task task) throws IllegalArgumentException {
        if(task.getTaskId() == null || task.getTaskId().isEmpty()) throw new IllegalArgumentException();
        taskMap.remove(task.getTaskId());
    }

    @Override
    public void removeAllTasksCollection(@NotNull final Collection<Task> taskCollection) throws EmptyCollectionException {
        if(taskCollection.isEmpty()) throw new EmptyCollectionException();
        taskMap.values().removeAll(taskCollection);
    }

    @Override
    public void removeAll() {
        taskMap.clear();
    }

    @Override
    public Task findOneByName(@NotNull final String userId, @NotNull final String taskName) throws IllegalArgumentException {
        if(userId.isEmpty() || taskName.isEmpty()) throw new IllegalArgumentException();
        for (@NotNull final Task task: taskMap.values()) {
            if(task.getName().equals(taskName) && task.getUserId().equals(userId)){
                return task;
            }
        }
        return null;
    }

    @Override
    public List<Task> sortByEndDate(@NotNull final String userId) throws IllegalArgumentException, EmptyCollectionException {
        if (userId.isEmpty()) throw new IllegalArgumentException();
        @NotNull final Collection<Task> taskCollection = findAllByUserId(userId);
        @NotNull final List<Task> taskList = new ArrayList<>(taskCollection);
        if (taskList.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Task> comparator = Comparator.comparing(Task::getDateEnd);
        taskList.sort(comparator);
        return taskList;
    }

    @Override
    public List<Task> sortByStartDate(@NotNull final String userId) throws IllegalArgumentException, EmptyCollectionException {
        if (userId.isEmpty()) throw new IllegalArgumentException();
        @NotNull final Collection<Task> taskCollection = findAllByUserId(userId);
        @NotNull final List<Task> taskList = new ArrayList<>(taskCollection);
        if (taskList.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Task> comparator = Comparator.comparing(Task::getDateStart);
        taskList.sort(comparator);
        return taskList;
    }

    @Override
    public List<Task> sortBySystemDate(@NotNull final String userId) throws IllegalArgumentException, EmptyCollectionException {
        if (userId.isEmpty()) throw new IllegalArgumentException();
        @NotNull final Collection<Task> taskCollection = findAllByUserId(userId);
        @NotNull final List<Task> taskList = new ArrayList<>(taskCollection);
        if (taskList.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Task> comparator = Comparator.comparing(Task::getDateSystem);
        taskList.sort(comparator);
        return taskList;
    }

    @Override
    public List<Task> sortByStatus(@NotNull final String userId) throws EmptyCollectionException, IllegalArgumentException {
        if (userId.isEmpty()) throw new IllegalArgumentException();
        @NotNull final Collection<Task> taskCollection = findAllByUserId(userId);
        if (taskCollection.isEmpty()) throw new EmptyCollectionException();
        @NotNull final List<Task> taskList = new ArrayList<>(taskCollection);
        if (taskList.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Task> comparator = Comparator.comparing(Task::getStatus);
        taskList.sort(comparator);
        return taskList;
    }

}

