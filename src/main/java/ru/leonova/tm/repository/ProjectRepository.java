package ru.leonova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.IProjectRepository;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.exeption.IllegalArgumentException;

import java.util.*;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    private final Map<String, Project> projectMap = new LinkedHashMap<>();

    @Override
    public void persist(@NotNull final Project project) {
        projectMap.put(project.getProjectId(), project);
    }

    @Override
    public Collection<Project> findAll() {
        return projectMap.values();
    }

    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String projectId) throws IllegalArgumentException, AccessException {
        if (userId.isEmpty() || projectId.isEmpty()) throw new IllegalArgumentException();
        if (!projectMap.get(projectId).getUserId().equals(userId)) throw new AccessException();
        return projectMap.get(projectId);

    }

    @Override
    public Collection<Project> findAllByUserId(@NotNull final String userId) throws IllegalArgumentException {
        if (userId.isEmpty()) throw new IllegalArgumentException();
        @NotNull final Collection<Project> projectCollection = new ArrayList<>();
        for (@NotNull final Project project : projectMap.values()) {
            if (project.getUserId().equals(userId)) {
                projectCollection.add(project);
            }
        }
        return projectCollection;
    }

    @Override
    public void updateProjectName(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name) throws IllegalArgumentException, AccessException {
        if (projectId.isEmpty() || name.isEmpty() || userId.isEmpty()) throw new IllegalArgumentException();
        @NotNull final Project project = projectMap.get(projectId);
        if(!project.getUserId().equals(userId)) throw new AccessException();
        project.setName(name);
    }

    public boolean isExist(@NotNull final String projectId) throws IllegalArgumentException {
        if (projectId.isEmpty()) throw new IllegalArgumentException();
        return projectMap.containsKey(projectId);
    }

    @Override
    public void merge(@NotNull final Project project) throws IllegalArgumentException, AccessException {
        if (project.getProjectId() == null || project.getProjectId().isEmpty()) throw new IllegalArgumentException();
        for (@NotNull final Project project1 : projectMap.values()) {
            if (project1.getProjectId().equals(project.getProjectId())) {
                updateProjectName(project.getUserId(), project1.getProjectId(), project.getName());
            } else {
                persist(project);
            }
        }
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Project project) throws IllegalArgumentException, AccessException {
        if (userId.isEmpty() || project.getProjectId() == null || project.getProjectId().isEmpty()) throw new IllegalArgumentException();
        if (!project.getUserId().equals(userId)) throw new AccessException();
        projectMap.remove(project.getProjectId());
    }

    @Override
    public void removeAllProjectsByUserId(@NotNull final String userId) throws IllegalArgumentException, EmptyCollectionException {
        if (userId.isEmpty()) throw new IllegalArgumentException();
        @NotNull final Collection<Project> projects = findAllByUserId(userId);
        if (projects==null || projects.isEmpty()) throw new EmptyCollectionException();
        projects.clear();
    }

    @Override
    public List<Project> sortedProjectsBySystemDate(@NotNull final String userId) throws IllegalArgumentException, EmptyCollectionException {
        if (userId.isEmpty()) throw new IllegalArgumentException();
        @NotNull final Collection<Project> projectCollection = findAll();
        @NotNull final List<Project> projects = new ArrayList<>(projectCollection);
        if (projects.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Project> comparator = Comparator.comparing(Project::getDateSystem);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public List<Project> sortedProjectsByStartDate(@NotNull final String userId) throws IllegalArgumentException, EmptyCollectionException {
        if (userId.isEmpty()) throw new IllegalArgumentException();
        @NotNull final Collection<Project> projectCollection = findAll();
        @NotNull final List<Project> projects = new ArrayList<>(projectCollection);
        if (projectCollection.isEmpty() || projects.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Project> comparator = Comparator.comparing(Project::getDateStart);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public List<Project> sortedProjectsByEndDate(@NotNull final String userId) throws IllegalArgumentException, EmptyCollectionException {
        if (userId.isEmpty()) throw new IllegalArgumentException();
        @NotNull final Collection<Project> projectCollection = Objects.requireNonNull(findAllByUserId(userId));
        @NotNull final List<Project> projects = new ArrayList<>(projectCollection);
        if (projects.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Project> comparator = Comparator.comparing(Project::getDateEnd);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public List<Project> sortedProjectsByStatus(@NotNull final String userId) throws EmptyCollectionException, IllegalArgumentException {
        if (userId.isEmpty()) throw new IllegalArgumentException();
        @NotNull final Collection<Project> projectCollection = Objects.requireNonNull(findAllByUserId(userId));
        @NotNull final List<Project> projects = new ArrayList<>(projectCollection);
        if (projectCollection.isEmpty() || projects.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Project> comparator = Comparator.comparing(Project::getStatus);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public Project searchProjectByName(@NotNull final String userId, @NotNull final String projectName) throws IllegalArgumentException {
        if (projectName.isEmpty() || userId.isEmpty()) throw new IllegalArgumentException();
        for (@NotNull final Project project : projectMap.values()) {
            if (project.getName().contains(projectName) && project.getUserId().equals(userId)) {
                return project;
            }
        }
        return null;
    }

    @Override
    public Project searchProjectByDescription(@NotNull final String userId, @NotNull final String description) throws IllegalArgumentException {
        if (description.isEmpty() || userId.isEmpty()) throw new IllegalArgumentException();
        for (@NotNull final Project project : projectMap.values()) {
            if (project.getDescription().contains(description) && project.getUserId().equals(userId)) {
                return project;
            }
        }
        return null;
    }

}
