package ru.leonova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.exeption.IllegalArgumentException;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class UserRepository extends AbstractRepository<User> implements ru.leonova.tm.api.repository.IUserRepository {

    final private Map<String, User> userMap = new LinkedHashMap();

    @Override
    public void persist(@NotNull final User user) {
        userMap.put(user.getUserId(), user);
    }

    @Override
    void merge(@NotNull final User user) throws IllegalArgumentException {
        @NotNull final String userId = user.getUserId();
        if (userId.isEmpty()) throw new IllegalArgumentException();
        if (isExist(userId)) userMap.get(userId).setLogin(user.getLogin());
        else persist(user);
    }

    private boolean isExist(@NotNull final String userId) throws IllegalArgumentException {
        if (userId.isEmpty()) throw new IllegalArgumentException();
        return userMap.containsKey(userId);
    }

    @Override
    public User findOne(@NotNull final String userId) throws IllegalArgumentException {
        if (userId.isEmpty()) throw new IllegalArgumentException();
        return userMap.get(userId);
    }

    @Override
    public Collection<User> findAll() {
        return userMap.values();
    }

    @Override
    public void remove(@NotNull final User user) throws IllegalArgumentException {
       if(user.getUserId().isEmpty()) throw new IllegalArgumentException();
       userMap.remove(user.getUserId());
    }

    @Override
    public void removeAll() {
        userMap.clear();
    }

}
