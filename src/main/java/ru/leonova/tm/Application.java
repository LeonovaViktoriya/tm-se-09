package ru.leonova.tm;

import ru.leonova.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}
