package ru.leonova.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.service.IProjectService;
import ru.leonova.tm.api.service.ITaskService;
import ru.leonova.tm.api.service.IUserService;
import ru.leonova.tm.command.AbstractCommand;

import java.util.List;

public interface ServiceLocator {
    @NotNull List<AbstractCommand> getCommands();

    @NotNull IProjectService getProjectService();

    @NotNull IUserService getUserService();

    @NotNull ITaskService getTaskService();
}
