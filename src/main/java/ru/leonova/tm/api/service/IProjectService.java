package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.exeption.IllegalArgumentException;

import java.util.Collection;
import java.util.List;

public interface IProjectService {
    Collection<Project> getList();

    void create(@NotNull String userId, @NotNull Project project) throws IllegalArgumentException, AccessException;

    void updateNameProject(@NotNull String userId, @NotNull String projectId, @NotNull String name) throws IllegalArgumentException, AccessException;

    Collection<Project> findAllByUserId(@NotNull String userId) throws IllegalArgumentException;

    void deleteProject(@NotNull String userId, @NotNull String projectId) throws IllegalArgumentException, AccessException;

    void deleteAllProject(String userId) throws IllegalArgumentException, EmptyCollectionException;

    List<Project> sortProjectsBySystemDate(@NotNull String userId) throws IllegalArgumentException, EmptyCollectionException;

    List<Project> sortProjectsByStartDate(@NotNull String userId) throws IllegalArgumentException, EmptyCollectionException;

    List<Project> sortProjectsByEndDate(@NotNull String userId) throws IllegalArgumentException, EmptyCollectionException;

    List<Project> sortProjectsByStatus(@NotNull String userId) throws EmptyCollectionException, IllegalArgumentException;

    Project searchProjectByName(@NotNull String userId, @NotNull String projectName) throws IllegalArgumentException;

    Project searchProjectByDescription(@NotNull String userId, @NotNull String description) throws IllegalArgumentException;
}
