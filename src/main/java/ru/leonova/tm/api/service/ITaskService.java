package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.exeption.IllegalArgumentException;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    void create(@NotNull  String userId, @NotNull final Task task) throws IllegalArgumentException;

    Collection<Task> getList();

    void updateTaskName(@NotNull String userId, @NotNull String taskId, @NotNull  String taskName) throws IllegalArgumentException;

    void deleteTasksByIdProject(@NotNull String userId, @NotNull String projectId) throws IllegalArgumentException, EmptyCollectionException, AccessException;

    Collection<Task> findAllByUserId(@NotNull String userId) throws IllegalArgumentException;

    Collection<Task> findAllByProjectId(@NotNull String projectId) throws IllegalArgumentException;

    boolean isEmptyTaskList();

    void deleteTask(@NotNull String userId, @NotNull String taskId) throws IllegalArgumentException, AccessException;

    void deleteAllTaskByUserId(@NotNull String userId) throws IllegalArgumentException, EmptyCollectionException;

    void deleteAllTaskByProjectId(@NotNull String userId, @NotNull String projectId) throws IllegalArgumentException, EmptyCollectionException, AccessException;

    Task findTaskByName(@NotNull String userId,@NotNull String taskName) throws IllegalArgumentException, AccessException;

    List<Task> sortTasksByEndDate(@NotNull String userId) throws IllegalArgumentException, EmptyCollectionException;

    List<Task> sortProjectsByStartDate(@NotNull String userId) throws IllegalArgumentException, EmptyCollectionException;

    List<Task> sortProjectsBySystemDate(@NotNull String userId) throws IllegalArgumentException, EmptyCollectionException;

    List<Task> sortTasksByStatus(@NotNull String userId) throws IllegalArgumentException, EmptyCollectionException;
}
