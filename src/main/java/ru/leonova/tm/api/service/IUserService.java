package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.exeption.IllegalArgumentException;

import java.util.Collection;

public interface IUserService {
    User getCurrentUser();

    void setCurrentUser(User currentUser);

    void create(@NotNull User user);

    User authorizationUser(@NotNull String login, @NotNull String password) throws IllegalArgumentException;

    boolean isAuth();

    User getById(@NotNull String userId) throws IllegalArgumentException;

    Collection<User> getList();

    void adminRegistration(@NotNull String admin, @NotNull String admin1) throws IllegalArgumentException;

    String md5Apache(@NotNull String password) throws IllegalArgumentException;

}
