package ru.leonova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.exeption.IllegalArgumentException;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository {
    void persist(@NotNull Task task) throws IllegalArgumentException;

    void merge(@NotNull Task task) throws IllegalArgumentException;

    Collection<Task> findAll();

    Task findOne(@NotNull String taskId) throws IllegalArgumentException;

    Collection<Task> findAllByUserId(@NotNull String userId) throws IllegalArgumentException;

    Collection<Task> findAllByProjectId(@NotNull String projectId) throws IllegalArgumentException;

    void remove(@NotNull Task t) throws IllegalArgumentException;

    boolean isExist(@NotNull String taskId) throws IllegalArgumentException;

    void removeAllTasksCollection(@NotNull Collection<Task> taskCollection) throws EmptyCollectionException;

    void removeAll();

    Task findOneByName(@NotNull String userId, @NotNull String taskName) throws IllegalArgumentException;

    List<Task> sortByEndDate(String userId) throws IllegalArgumentException, EmptyCollectionException;

    List<Task> sortByStartDate(String userId) throws IllegalArgumentException, EmptyCollectionException;

    List<Task> sortBySystemDate(String userId) throws IllegalArgumentException, EmptyCollectionException;

    List<Task> sortByStatus(String userId) throws EmptyCollectionException, IllegalArgumentException;
}
