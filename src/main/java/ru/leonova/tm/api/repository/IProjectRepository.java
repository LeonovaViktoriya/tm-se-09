package ru.leonova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.exeption.IllegalArgumentException;

import java.util.Collection;
import java.util.List;

public interface IProjectRepository {

    void persist(@NotNull Project project);

    Collection<Project> findAll();

    Project findOneById(@NotNull String userId, @NotNull String projectId) throws IllegalArgumentException, AccessException;

    void merge(@NotNull Project project) throws IllegalArgumentException, AccessException;

    void remove(@NotNull String userId, @NotNull Project project) throws IllegalArgumentException, AccessException;

    void removeAllProjectsByUserId(@NotNull String userId) throws IllegalArgumentException, EmptyCollectionException;

    Collection<Project> findAllByUserId(@NotNull String userId) throws IllegalArgumentException;

    void updateProjectName(@NotNull String userId, @NotNull String projectId, @NotNull String name) throws IllegalArgumentException, AccessException;

    boolean isExist(@NotNull String projectId) throws IllegalArgumentException;

    List<Project>  sortedProjectsBySystemDate(@NotNull String userId) throws IllegalArgumentException, EmptyCollectionException;

    List<Project> sortedProjectsByStartDate(@NotNull String userId) throws IllegalArgumentException, EmptyCollectionException;

    List<Project> sortedProjectsByEndDate(@NotNull String userId) throws IllegalArgumentException, EmptyCollectionException;

    List<Project> sortedProjectsByStatus(@NotNull String userId) throws EmptyCollectionException, IllegalArgumentException;

    Project searchProjectByName(@NotNull String userId, @NotNull String projectName) throws IllegalArgumentException;

    Project searchProjectByDescription(@NotNull String userId, @NotNull String description) throws IllegalArgumentException;
}
