package ru.leonova.tm.api.repository;

import ru.leonova.tm.entity.User;
import ru.leonova.tm.exeption.IllegalArgumentException;

import java.util.Collection;

public interface IUserRepository {
    void persist(User user);

    User findOne(String userId) throws IllegalArgumentException;

    Collection<User> findAll();

    void remove(User user) throws IllegalArgumentException;

    void removeAll();
}
